$(document).ready(function(){
    $('.box-kho-cau-hoi .show-aks').click(function(){
        var itemclass = $(this).attr('data-class')
        var content = $(this).attr('data-content')
        var url = $(this).attr('data-url')
        $('body').addClass('overlay')
        $('.box-kho-cau-hoi .box-cau-hoi').addClass(itemclass)
        $('.box-kho-cau-hoi .box-cau-hoi').addClass('active')
        $('.box-kho-cau-hoi .box-cau-hoi').find('.content').html(content)
        $('.box-kho-cau-hoi .box-cau-hoi').find('.btn').attr('href', url)
    })
    $(document).mouseup(function (e) {
		var container = $(".box-kho-cau-hoi .box-cau-hoi");
		if (!container.is(e.target) &&
			container.has(e.target).length === 0) {
			$('.box-kho-cau-hoi .box-cau-hoi').removeClass('active');
			$('.box-kho-cau-hoi .box-cau-hoi').removeClass('ask-bg-1');
			$('.box-kho-cau-hoi .box-cau-hoi').removeClass('ask-bg-2');
			$('.box-kho-cau-hoi .box-cau-hoi').removeClass('ask-bg-3');
			$('.box-kho-cau-hoi .box-cau-hoi').removeClass('ask-bg-4');
			$('.box-kho-cau-hoi .box-cau-hoi').removeClass('ask-bg-5');
            $('body').removeClass('overlay')
		}
	});

})